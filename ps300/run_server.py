from __future__ import absolute_import
import sys

from tango.server import run

from ps300.ps300 import PS300


def main(args=None, **kwargs):
    """
    Run device server for PS300 device class.

    :param list args: list of command line arguments [default: None, \
    meaning  use sys.argv]
    :param kwargs: for more info look for tango.server.run doc in PyTango
    :return: None
    """
    run((PS300,), args=args, **kwargs)


def run_ps300(args=None, **kwargs):
    """
    Run device server for PS300 device class.

    :param list args: list of command line arguments [default: None, \
    meaning  use sys.argv]
    :param kwargs: for more info look for tango.server.run doc in PyTango
    :return: None
    """
    run((PS300,), args=args, **kwargs)


if __name__ == '__main__':
    main(["PS300"] + sys.argv[1:])

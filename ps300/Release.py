"""
This module contains metadata of PS300 project
"""

__name__ = "dev-ps300"
__author__ = "Wojciech Kitka"
__author_email__ = "wojciech.kitka@uj.edu.pl"
__version__ = "1.0.0"
__license__ = "UJ"
__url__ = "https://gitlab.m.cps.uj.edu.pl/CSIT/Beamlines/dev-solaris-ps300.git"
__description__ = "PS300 tango class"

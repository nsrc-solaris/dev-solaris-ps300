from threading import Lock
from decimal import Decimal

from facadedevice import Facade, proxy_command
from tango import AttrWriteType, DevState, DispLevel, DebugIt
from tango.server import attribute, command, device_property


class PS300(Facade):

    lock = False

    def safe_init_device(self):
        super(PS300, self).safe_init_device()
        self.lock = Lock()
        self.set_state(DevState.ON)
        self.set_status("The device is running.")

    # Properties
    GPIBWriteReadCommand = device_property(dtype=str)

    GPIBWriteCommand = device_property(dtype=str)

    # Local attributes
    @attribute(dtype=float,
               access=AttrWriteType.READ,
               display_level=DispLevel.OPERATOR,
               label="Output current",
               doc="The actual output current. This is the same value as shown"
                   " on the front panel meter. Note: As with the front panel "
                   "meters, this value takes about a second to stabilize after"
                   " a change in current.")
    def Current(self):
        return float(self.ps_command("IOUT?"))

    @attribute(dtype=float,
               access=AttrWriteType.READ,
               display_level=DispLevel.OPERATOR,
               label="Output voltage",
               doc="The actual output voltage. This is the same value as shown"
                   " on the front panel meter. Note: As with the front panel "
                   "meters, this value takes about a second to stabilize after"
                   " a change in voltage.")
    def Voltage(self):
        return float(self.ps_command("VOUT?"))

    @attribute(dtype=float,
               access=AttrWriteType.READ,
               display_level=DispLevel.OPERATOR,
               label="VSET mode",
               doc="The value 0 means that the voltage value is controlled by "
                   "the front panel setting, while the value 1 indicates that "
                   "the output is controlled by therear panel voltage control "
                   "input. Note that the setting mode may only be changed by "
                   "setting the rear panel switch.")
    def VsetMode(self):
        return float(self.ps_command("SMOD?"))

    @attribute(dtype=float,
               display_level=DispLevel.OPERATOR,
               access=AttrWriteType.READ_WRITE,
               fset="write_current_limit",
               label="Current limit",
               doc="The ILIM command sets the value of the current limit to x."
                   " The value x is a floating point value with the units of "
                   "Amps (e.g. 1 mA = 1E-3). The ILIM? query returns the "
                   "current limit setting")
    def CurrentLimit(self):
        return float(self.ps_command("ILIM?"))

    @attribute(dtype=float,
               display_level=DispLevel.OPERATOR,
               access=AttrWriteType.READ_WRITE,
               fset="write_voltage_limit",
               label="Voltage limit",
               doc="The VLIM command sets the value of the voltage limit to x "
                   "where x is the value in Volts. The sign of value x MUST "
                   "match the PS300's polarity setting. The VLIM? query returns"
                   " the present VLIM setting. As with front panel control, the"
                   " VLIM value must be greater than or equal to the VSET value"
                   " or an execution error will be returned.")
    def VoltageLimit(self):
        return float(self.ps_command("VLIM?"))

    @attribute(dtype=float,
               display_level=DispLevel.OPERATOR,
               access=AttrWriteType.READ_WRITE,
               fset="write_voltage_setpoint",
               label="Voltage setpoint",
               doc="The VSET command sets the value of the voltage set to x if"
                   " front panel control is enabled. If rear panel control is "
                   "enabled, an error is returned. The value x is a number in "
                   "the units of Volts and the sign of the number MUST match "
                   "the PS300's polarity setting. The VSET? query returns the "
                   "current VSET value. As with front panel control, the VSET "
                   "value must be less than or equal to to VLIM value or an "
                   "execution error will be returned")
    def VoltageSetpoint(self):
        return float(self.ps_command("VSET?"))

    @attribute(dtype=float,
               display_level=DispLevel.OPERATOR,
               access=AttrWriteType.READ_WRITE,
               fset="write_current_trip",
               label="Current trip",
               doc="The ITRP command sets the value of the current trip to x. "
                   "The value x is a floating point value with the units of "
                   "Amps (e.g. 1 mA = 1E-3")
    def CurrentTrip(self):
        return float(self.ps_command("ITRP?"))

    @attribute(dtype=int,
               min_value=0,
               max_value=1,
               display_level=DispLevel.OPERATOR,
               access=AttrWriteType.READ_WRITE,
               fset="write_trip_reset_mode",
               label="Trip reset mode",
               doc="The TMOD command sets the trip reset mode. The value i = 0"
                   " sets manual trip reset, while the value i = 1 sets the "
                   "trip reset mode to auto.")
    def CurrentTrip(self):
        return float(self.ps_command("TMOD?"))

    # Commands
    @command(display_level=DispLevel.EXPERT,
             doc_in="The HVON command turns the high voltage ON provided that "
                    "the front panel high voltage switch is not in the OFF "
                    "position.")
    @DebugIt()
    def TurnHVOn(self):
        self.UserWriteCommand("HVON")

    @command(display_level=DispLevel.EXPERT,
             doc_in="The HVOF command turns the high voltage OFF")
    @DebugIt()
    def TurnHVOf(self):
        self.UserWriteCommand("HVOF")

    @command(display_level=DispLevel.EXPERT,
             dtype_in=int,
             doc_in="The *RCL command recalls stored setting i. Setting 0 "
                    "recalls the default settings. If the stored setting is "
                    "corrupted, an error is returned.")
    @DebugIt()
    def RecallSettings(self, value):
        if -1 < value < 10:
            self.UserWriteCommand("*RCL %d" % value)

    @command(display_level=DispLevel.EXPERT,
             dtype_in=int,
             doc_in="The *SAV command stores the present setup as setting i. i "
                    "may range from 1 to 9.")
    @DebugIt()
    def SaveSettings(self, value):
        if -1 < value < 10:
            self.UserWriteCommand("*SAV %d" % value)

    @command(display_level=DispLevel.EXPERT,
             doc_in="The TCLR command clears any voltage or current trips")
    @DebugIt()
    def ClearTrips(self):
        self.UserWriteCommand("TLCR")

    # Proxy commands
    UserReadWriteCommand = proxy_command(
        dtype_in=str,
        dtype_out=str,
        property_name="GPIBWriteReadCommand",
        doc_in="User command",
        doc_out="Response")

    UserWriteCommand = proxy_command(
        dtype_in=str,
        property_name="GPIBWriteCommand",
        doc_in="User command")

    # write methods
    def write_current_limit(self, value):
        self.UserWriteCommand('ILIM %.2E' % Decimal(value))

    def write_voltage_limit(self, value):
        self.UserWriteCommand('VLIM %.2E' % Decimal(value))

    def write_voltage_setpoint(self, value):
        self.UserWriteCommand('VLIM %f' % value)

    def write_current_trip(self, value):
        self.UserWriteCommand('ITRP %.2E' % Decimal(value))

    def write_trip_reset_mode(self, value):
        self.UserWriteCommand('TMOD %d' % value)

    # other methods
    def ps_command(self, cmd):
        with self.lock:
            try:
                return self.UserReadWriteCommand(cmd)
            except Exception as e:
                self.error_stream("Problem with connection to the PS300: "
                                  + str(e))


# run server
run = PS300.run_server

if __name__ == '__main__':
    run()

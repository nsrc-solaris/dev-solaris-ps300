from setuptools import find_packages, setup

from ps300.Release import __author__, __author_email__, \
    __description__, __license__, __name__, __url__, __version__

setup(
    name=__name__,
    author=__author__,
    author_email=__author_email__,
    version=__version__,
    license=__license__,
    description=__description__,
    long_description=open("README.md").read(),
    url=__url__,
    packages=find_packages(),
    package_data={"ps300": [],},
    data_files=[],
    entry_points={
        "console_scripts": ["PS300 = ps300.run_server:run_ps300"]
    },
    project_urls = {
        "Bug Reports":
            "https://gitlab.m.cps.uj.edu.pl/CSIT/Beamlines/dev-solaris-ps300/issues",
        "Documentation": "https://csit.docs.cps.uj.edu.pl/",
        "Source": "https://gitlab.m.cps.uj.edu.pl/CSIT/Beamlines/dev-solaris-ps300",
    }
)
